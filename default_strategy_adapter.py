from configuration import settings

class DefaultStrategyAdapter:
    def __init__(self, player_id, Strategy, strategy_update, serializer):
        self.player_id = player_id
        self.strategy_update = strategy_update
        self.serializer = serializer
        self.strategy = Strategy()

    def setup(self):
        pass

    def update(self, game_state_data):
        game_state = self.serializer.parse_game_state(game_state_data)
        moving = self.strategy_update(self.strategy, game_state)
        return self.serializer.serialize_moving(moving)

    def goodbye(self):
        pass
