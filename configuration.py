import importlib

settings = importlib.import_module('default_settings')

def configure():
    import sys
    sys.path.append(settings.GAME_SERVER_CORE_PATH)
    sys.path.append(settings.MECHANICS_PATH)
