GAME_SERVER_CORE_PATH = '..'
MECHANICS_PATH = '../mechanics/'

def STRATEGY_ADAPTERS():
    from game_server_core.network_strategy_adapter import NetworkStrategyAdapter
    return [
        NetworkStrategyAdapter(player_id=1, port=12345),
        'default',
    ]

MECHANIC = 'test_mech'

try:
    from settings import *
except ImportError:
    print('settings.py not found. Using default settings.')
