import importlib
import pygame
import sys

import configuration
configuration.configure()

from configuration import settings
from game_server_core.game_server_core import GameServerCore
from default_strategy_adapter import DefaultStrategyAdapter

drawer = importlib.import_module(settings.MECHANIC + '.drawing.drawer')
mechanic = importlib.import_module(settings.MECHANIC + '.game.mechanic')
strategy_main = importlib.import_module(settings.MECHANIC + '.default_strategy.main')
serializer = importlib.import_module(settings.MECHANIC + '.default_strategy.serializer')
my_strategy = importlib.import_module(settings.MECHANIC + '.default_strategy.my_strategy')

class DebugRunner(object):
    def start(self):
        pygame.init()
        self.screen = pygame.display.set_mode((900, 900))
        self.font = pygame.font.SysFont("monospace", 24, bold=True)
        pygame.display.set_caption('wait players connection..')
        strategy_adapters = []
        strategy_adapters_setting = settings.STRATEGY_ADAPTERS()
        adapters_count = len(strategy_adapters_setting)
        for num, strategy_adapter in enumerate(strategy_adapters_setting):
            if strategy_adapter == 'default':
                strategy_adapters.append(DefaultStrategyAdapter(
                    num + adapters_count,  # for unique id
                    my_strategy.Strategy,
                    strategy_main.update,
                    serializer
                ))
            else:
                strategy_adapters.append(strategy_adapter)
        server = GameServerCore(
            mechanic.Mechanic,
            strategy_adapters,
            self.on_world_update
        )
        server.start()


    def on_world_update(self, world):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        drawer.draw(world, self.screen, pygame)

        pygame.display.flip()
        # pygame.time.delay(33)
        pygame.display.set_caption('game is running')


if __name__ == '__main__':
    DebugRunner().start()
